

//[Section] Syntax, Statements and Comments

	// Statements in programming are instructions that we tell the computer to perform
	// Js statements usually end with semicolon (;) doesn't really affect the statement but only for good practices only
	// Semicolons are not reauired in JS, it is used for practice and show where a statement ends
	// A syntax in programming, it is the set of rules of rules that we describe how statements must be constructed
	// All lines/blocks of code should be weitten in speciffic programmed manner work. This is due to how these codes where essentially programmed to function in a certain manner.

	// comments are parts of the code that gets ignored by the language.
	// comments are meant to describe the written code.

	/* 
		ctrl shift forward-slash - multiple line comment
	*/

//[Section] Variables

	// Variables are used to contain data.
	// Any information that is used by an application is stored in what we call the "memory"
	// when we create variables, certain portions of a device's memory is given a name that we call variables
	// this makes it easier for us to associate info stored in our divices to actual "names" about information.

	// Delaring Variables
	// Declaring variables - tells our devices that a variable name is created and is ready to store data.
	// declaring a variable w/o gibing it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined.

	/* 
		Syntax
			let / const variableName;
	*/

	// definitions //
		//console.log() is useful for printing values of variables of certain results of code into Google Chrome browser's console.
		// constant use of this tjroughout developing an application will save us time and builds good habit in always checking for the output of our code.


let myVariable = "Ada Lovelace";
console.log(myVariable);	

/*
	Guides in writing variables;
		1. use the let keyword followed by the variable name of your choose and use the assignment operator (=) to assign value.
		2.Variables names should start with a lowercase char. camelCase like.
		3. for constant var, use the 'const' keyword. 
			using let: we can change the value of the variable.
			using names should be indicative (descriptive) of the val being stored to avoid confusion.
		4.
*/

// Declare and Initialize Var
// Initializing variables - the instance when a variable is given it's initial starting value.

	/*
		Syntax
			let/const varialbeName = value;
	*/

let productName = "desktop conputer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);


	// In the context of certain applictaions, some var/ info are constant and should not be changed.

	// In this example, the interest rate for a loan, saving account or a mortgage must not be change due to real world concerns.

const interest = 3.539;
console.log(interest);

	// Reassinging variable values
	// Reassinging a variable means changing it's initial or previous value into another value.
	// Syntax
		//varialbleName

productName = "Laptop";
console.log(productName);	

	// let variable cannot be re-declare within its scope but its value can be updated.

	// values of constants cannot be changed and will simply return an error.

// interest = 4.489; // this'll prompt an error
// console.log(interest)

	//  reassigning a variable and initializing variable


let supplier; 

supplier = "Zuitt Store"; // Initializing (initial value)

supplier = "Zuitt merch" // re-Assigning/ment

// const name ; // const should always come with a value - this will prompt an error

const name = "Gerge Alfred Cabaccang";

// var vs let/const
	// var - is also used in declaring variable, but var is an EcmaScript1 feature [ES1(Javascript 1997)]
	// let and const was introduced as a new feature in ES6 (2016)

	// what makes let/const different from var?
	// in var, you can assign / initialize a value even without declaring (it first)
	// old way of coding and has a lot of negatives

	a = 5;
	console.log(a);
	var a;

/* 
	this'll prompt an error

	b = 6;
	console.log(b);
	let b;

*/

	// let/const local/globe
	// let and const are blocked.
	// a block is a chunk of code bounded by {}. A block in curly braces. Anything within the curly braces is a block.

	let outerVariable = "hello from the other side"; // this will be automaticaly assigned globaly since it is declared/assign outside a block.

	{
		let innerVariable = "hello from the block";
		console.log(innerVariable);
		console.log(outerVariable); // will not prompt an error since it is declared globaly. It is not bound inside a block which automaticaly assigns it to be gloabaly accessible
	}

	console.log(outerVariable);
	// console.log(innerVariable); // this'll prompt an error due to it being inside a block //privated.

//  Multiple variable declarations

//	multiple variables can be declared in one line.

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword

// const let = "hello"; // this'll prompt an error since let is a reserved key word
// console.log(let);

//[Section] Data types
	//strings are series of characters that create a word, a phrase, a sentence or anything related to creating text.
	// strings in JS can be written using either sing (') and double (") quote.

let country = "Philippines";
let province = "Metro Manila";
	// Concatinate

let fullAdd = province + ", " + country;
console.log(fullAdd);

let greeting = "I live in the " + country;
console.log(greeting);

	// the scape characters (\) in strings in combinations w/ other characters can produce different effects.
	// \n refers to creating a new line or next line

let message = "John's employees\n went home";
console.log(message);
	// let message = 'John\'s employees went home'; \ scape char

	// Nmbers
	// Integers/ whole number
	// int and strings have different font color

let count = "26";
let headCount = 26;

console.log(count);
console.log(headCount);

	// Decimal Numbers / Fractions
let grade = 98.2;
console.log(grade);

	// Exponential Notation;
let planetDistance = 2e10;
console.log(planetDistance);

	// Type coercion
console.log("John's grade last quarter is " + grade);
console.log(count + headCount);

	// Boolean - troo or fools

let isMarried = false;
let inGoodConduct = true;

console.log(isMarried);
console.log(inGoodConduct);

console.log("is Married " + isMarried);

	// Arrays - can store more than 1 value
	// can also store different data types.

	/*
		similar data types
			syntax
			let/const arrayName = [elemetA , elementB, .....];
	*/

	// same data types

let grades = [89.7, 80.2, 89.5, 94.5];
console.log(grades[1]); // access specific array element
console.log(grades);

	// different data types
	// storing different w/ different data types in an array will not prompt an erro but is not recommended due to it not making a sense in the context of programming
let detail	= ["John", "Smith", 1111];
console.log(detail);

	// Objects
	// are another special kind of data type that's used to mimic real word objects
	// used to create complex data that contains pieces of info that are relevant to each other

	/*
		let/const objectName = {
			propertyA: val1,
			propertyB: val2,
		};
	*/

let objectName = {
	name: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["099929" , "0947474"],
	address:{
		houseNum: '345',
		city: "Manila"
	}
}

console.log(objectName);
console.log(objectName.address.houseNum);

	// typeof operator is used to determine the type of data or the value of a variable.
console.log(typeof objectName);
console.log(typeof grade);
	// will display obj. because it is a special type of object with methods and funtions to manipulate it.
console.log(typeof detail); 

	/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

    */

const anime = ['one piece', 'one punch man', 'attack on titan'];
console.log(anime);

	// anime = ['kimetsu no yaiba']; // will prompt error
anime[0] = 'kimetsu no yaiba'; // will not prompt because we are accessing only the values not the numbers on an array
console.log(anime);
anime[5] = 'dxd';
console.log(anime);

	// Null
	// used tp intentionally expressed the absence of a value in a variable/ initialization
	// means that a data type was assigned to a variable but it does not hold any value/amount or is nullified.

let spouse = null;
console.loge(spouse);

	// undefined is a state of a variable that has been declared but without value.
let fullName;
console.log(fullName);
